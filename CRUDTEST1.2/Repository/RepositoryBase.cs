﻿using CRUDTEST1._2.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRUDTEST1._2.Repository
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class, IEntity
    {
        private readonly DataContext _dataContext;
        public RepositoryBase(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public async Task<bool> CreateAsync(TEntity entity)
        {
            await _dataContext.Set<TEntity>().AddAsync(entity);
            var created = await _dataContext.SaveChangesAsync();
            return created > 0;
        }

        public async Task<bool> DeleteAsync(Guid Id)
        {
            var entity = await GetByIdAsync(Id);
            if (entity == null)
                return false;

            _dataContext.Set<TEntity>().Remove(entity);
            var deleted = await _dataContext.SaveChangesAsync();
            return deleted > 0;
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dataContext.Set<TEntity>().AsNoTracking();
        }

        public async Task<TEntity> GetByIdAsync(Guid Id)
        {
            return await _dataContext.Set<TEntity>().AsNoTracking()
                          .FirstOrDefaultAsync(e =>  e.Id == Id);
        }

        public Task<bool> UpdateAsync(Guid Id, TEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
