﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace CRUDTEST1._2.Repository
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetAll();
        Task<TEntity> GetByIdAsync(Guid Id);
        Task<bool> CreateAsync(TEntity entity);
        Task<bool> UpdateAsync(Guid Id, TEntity entity);
        Task<bool> DeleteAsync(Guid Id);

    }
}
 