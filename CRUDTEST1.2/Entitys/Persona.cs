﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CRUDTEST1._2.Entitys
{
    public class Persona
    {
        [Key]
        public Guid Id { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Cedula { get; set; }

    
        public virtual List<Numero> Numeros { get; set; }

    }
}
