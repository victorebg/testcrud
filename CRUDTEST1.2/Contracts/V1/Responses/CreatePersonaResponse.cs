﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRUDTEST1._2.Contracts.V1.Responses
{
    public class CreatePersonaResponse
    {
        public Guid Id { get; set; }
    }
}
