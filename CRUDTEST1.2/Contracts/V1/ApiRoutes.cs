﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRUDTEST1._2.Contracts.V1
{
    public static class ApiRoutes
    {
        public const string Root = "api";
        public const string Version = "v1";
        public const string Base = Root + "/" + Version;


        public static class Persona
        {
            public const string GetAll = Base + "/persona";
            public const string Create = Base + "/persona";
            public const string Get = Base + "/{personaId}";
            public const string Delete = Base + "/{personaId}";
            public const string Update = Base + "/{personaId}";
        }

    }
}
