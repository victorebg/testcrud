﻿using CRUDTEST1._2.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRUDTEST1._2.Contracts.V1.Requests
{
    public class CreatePersonaRequest
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Cedula { get; set; }
        public List<CreateNumeroRequest> Numeros { get; set; }
    }
}
