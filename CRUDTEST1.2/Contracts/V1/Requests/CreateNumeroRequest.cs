﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRUDTEST1._2.Contracts.V1.Requests
{
    public class CreateNumeroRequest
    {
        public string Numero { get; set; }
        public string TipoNumero { get; set; }

    }
}
