﻿using CRUDTEST1._2.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRUDTEST1._2.Services.Interfaces
{
    public interface IPersonaService
    {
        Task<List<Persona>> GetPersonaAsync();
        Task<Persona> GeyPersonaByIdAsync(Guid Id);
        Task<bool> CreatePersonaAsync(Persona persona);
        Task<bool> DeletePersonaAsync(Guid Id);
        Task<bool> UpdatePersonaAsync(Persona post);
    }
}
