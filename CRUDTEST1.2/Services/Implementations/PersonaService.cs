﻿using CRUDTEST1._2.Data;
using CRUDTEST1._2.Entitys;
using CRUDTEST1._2.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRUDTEST1._2.Services.Implementations
{
    public class PersonaService : IPersonaService
    {
        private readonly DataContext _dataContext;

        public PersonaService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public async Task<bool> CreatePersonaAsync(Persona persona)
        {
            await _dataContext.Personas.AddAsync(persona);
            var created = await _dataContext.SaveChangesAsync();
            return created > 0;
        }

        public async Task<bool> DeletePersonaAsync(Guid Id)
        {
            var persona = await GeyPersonaByIdAsync(Id);
            if (persona == null)
                return false;

            _dataContext.Personas.Remove(persona);
            var deleted = await _dataContext.SaveChangesAsync();
            return deleted > 0;
        }

        public async Task<List<Persona>> GetPersonaAsync()
        {
            return await _dataContext.Personas
                .Include(x => x.Numeros)
                .ToListAsync();
        }

        public async Task<Persona> GeyPersonaByIdAsync(Guid Id)
        {
            return await _dataContext.Personas.Include(x => x.Numeros)
                .SingleOrDefaultAsync(x => x.Id == Id);
            
        }

        public async Task<bool> UpdatePersonaAsync(Persona post)
        {
            _dataContext.Personas.Update(post);
            var updated = await _dataContext.SaveChangesAsync();
            return updated > 0;
        }
    }
}
