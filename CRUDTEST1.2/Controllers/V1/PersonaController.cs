﻿using CRUDTEST1._2.Contracts.V1;
using CRUDTEST1._2.Contracts.V1.Requests;
using CRUDTEST1._2.Contracts.V1.Responses;
using CRUDTEST1._2.Entitys;
using CRUDTEST1._2.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRUDTEST1._2.Controllers.V1
{
    public class PersonaController : Controller
    {
        private readonly IPersonaService _personaService;

        public PersonaController(IPersonaService personaService)
        {
            _personaService = personaService;

        }

        [HttpGet(ApiRoutes.Persona.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _personaService.GetPersonaAsync());
        }

        [HttpGet(ApiRoutes.Persona.Get)]
        public async Task<IActionResult> Get([FromRoute] Guid personaId)
        {
            var persona = await _personaService.GeyPersonaByIdAsync(personaId);
            return Ok(persona);
        }

        [HttpPost(ApiRoutes.Persona.Create)]
        public async Task<IActionResult> Create([FromBody] CreatePersonaRequest createPersonaRequest)
        {
            var personaId = new Guid();
            var numeroId = new Guid();

            var persona = new Persona
            {
                Id = personaId,
                Nombres = createPersonaRequest.Nombres,
                Apellidos = createPersonaRequest.Apellidos,
                Cedula = createPersonaRequest.Cedula,
                Numeros = createPersonaRequest.Numeros.Select(x => new Numero { NumeroContacto = x.Numero, TipoNumero = x.TipoNumero, Id = numeroId }).ToList()                
            };

            var created = await _personaService.CreatePersonaAsync(persona);

            if (!created)
                return BadRequest();

            var baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.ToUriComponent()}";
            var locationUrl = baseUrl + "/" + ApiRoutes.Persona.Get.Replace("{personaId}", persona.Id.ToString());

            var response = new CreatePersonaResponse
            {
                Id = persona.Id
            };

            return Created(locationUrl, response);
        }

        [HttpPut(ApiRoutes.Persona.Update)]
        public async Task<IActionResult> Update([FromRoute] Guid personaId,[FromBody] UpdatePersonaRequest updatePersonaRequest)
        {
            var persona = new Persona
            {
                Id = personaId,
                Nombres = updatePersonaRequest.Nombres,
                Apellidos = updatePersonaRequest.Apellidos,
                Cedula = updatePersonaRequest.Cedula
            };

            var updated = await _personaService.UpdatePersonaAsync(persona);
            if (updated)
                return Ok(persona);
            return NotFound();
        }


        [HttpDelete(ApiRoutes.Persona.Delete)]
        public async Task<IActionResult> Delete([FromRoute] Guid personaId)
        {
            var deleted = await _personaService.DeletePersonaAsync(personaId);

            if (deleted)
                return Ok();
            return NotFound();
        }
    }
}
