﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CRUDTEST1._2.Data.Migrations
{
    public partial class test2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Personas_Numeros_PersonaId",
                table: "Personas");

            migrationBuilder.DropIndex(
                name: "IX_Personas_PersonaId",
                table: "Personas");

            migrationBuilder.DropColumn(
                name: "PersonaId",
                table: "Personas");

            migrationBuilder.DropColumn(
                name: "PersonaId",
                table: "Numeros");

            migrationBuilder.AddColumn<Guid>(
                name: "PersonasId",
                table: "Numeros",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Numeros_PersonasId",
                table: "Numeros",
                column: "PersonasId");

            migrationBuilder.AddForeignKey(
                name: "FK_Numeros_Personas_PersonasId",
                table: "Numeros",
                column: "PersonasId",
                principalTable: "Personas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Numeros_Personas_PersonasId",
                table: "Numeros");

            migrationBuilder.DropIndex(
                name: "IX_Numeros_PersonasId",
                table: "Numeros");

            migrationBuilder.DropColumn(
                name: "PersonasId",
                table: "Numeros");

            migrationBuilder.AddColumn<Guid>(
                name: "PersonaId",
                table: "Personas",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "PersonaId",
                table: "Numeros",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Personas_PersonaId",
                table: "Personas",
                column: "PersonaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Personas_Numeros_PersonaId",
                table: "Personas",
                column: "PersonaId",
                principalTable: "Numeros",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
