﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CRUDTEST1._2.Data.Migrations
{
    public partial class telefonomulti : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Numeros_Personas_PersonaId",
                table: "Numeros");

            migrationBuilder.DropIndex(
                name: "IX_Numeros_PersonaId",
                table: "Numeros");

            migrationBuilder.AddColumn<Guid>(
                name: "PersonaId",
                table: "Personas",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Personas_PersonaId",
                table: "Personas",
                column: "PersonaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Personas_Numeros_PersonaId",
                table: "Personas",
                column: "PersonaId",
                principalTable: "Numeros",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Personas_Numeros_PersonaId",
                table: "Personas");

            migrationBuilder.DropIndex(
                name: "IX_Personas_PersonaId",
                table: "Personas");

            migrationBuilder.DropColumn(
                name: "PersonaId",
                table: "Personas");

            migrationBuilder.CreateIndex(
                name: "IX_Numeros_PersonaId",
                table: "Numeros",
                column: "PersonaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Numeros_Personas_PersonaId",
                table: "Numeros",
                column: "PersonaId",
                principalTable: "Personas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
