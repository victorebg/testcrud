﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CRUDTEST1._2.Data.Migrations
{
    public partial class telefonoentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Numeros",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    NumeroContacto = table.Column<string>(nullable: true),
                    TipoNumero = table.Column<string>(nullable: true),
                    PersonaId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Numeros", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Numeros_Personas_PersonaId",
                        column: x => x.PersonaId,
                        principalTable: "Personas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Numeros_PersonaId",
                table: "Numeros",
                column: "PersonaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Numeros");
        }
    }
}
