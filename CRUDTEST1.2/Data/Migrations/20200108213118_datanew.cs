﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CRUDTEST1._2.Data.Migrations
{
    public partial class datanew : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Numeros_Personas_PersonasId",
                table: "Numeros");

            migrationBuilder.RenameColumn(
                name: "PersonasId",
                table: "Numeros",
                newName: "PersonaId");

            migrationBuilder.RenameIndex(
                name: "IX_Numeros_PersonasId",
                table: "Numeros",
                newName: "IX_Numeros_PersonaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Numeros_Personas_PersonaId",
                table: "Numeros",
                column: "PersonaId",
                principalTable: "Personas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Numeros_Personas_PersonaId",
                table: "Numeros");

            migrationBuilder.RenameColumn(
                name: "PersonaId",
                table: "Numeros",
                newName: "PersonasId");

            migrationBuilder.RenameIndex(
                name: "IX_Numeros_PersonaId",
                table: "Numeros",
                newName: "IX_Numeros_PersonasId");

            migrationBuilder.AddForeignKey(
                name: "FK_Numeros_Personas_PersonasId",
                table: "Numeros",
                column: "PersonasId",
                principalTable: "Personas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
