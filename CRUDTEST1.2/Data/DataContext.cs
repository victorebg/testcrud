﻿using System;
using System.Collections.Generic;
using System.Text;
using CRUDTEST1._2.Entitys;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CRUDTEST1._2.Data
{
    public class DataContext : IdentityDbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }


        public DbSet<Persona> Personas { get;set;}

        public DbSet<Numero> Numeros { get; set; }



    }
}
