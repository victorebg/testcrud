﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace CRUDTEST1._2.Core.Repository
{
    public interface IRepositoryBase<TEntity,TId> where TEntity : class where TId : Type
    {
        IQueryable<TEntity> GetAll();
        Task<TEntity> GetById(TId Id);
        Task<bool> Create(TEntity entity);
        Task<bool> Update(TId Id, TEntity entity);
        Task<bool> Delete(TId Id);

    }
}
 