﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace CRUDTEST1._2.Core.Repository
{
    public class RepositoryBase<TEntity, TId> : IRepositoryBase<TEntity, TId> where TEntity : class where TId : Type
    {
        private readonly DataContext
        public RepositoryBase()
        {
                
        }
        public Task<bool> Create(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(TId Id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<TEntity> GetAll()
        {
            throw new NotImplementedException();
        }

        public Task<TEntity> GetById(TId Id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Update(TId Id, TEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
